require 'test_helper'

class OfferPhotosControllerTest < ActionController::TestCase
  setup do
    @offer_photo = offer_photos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:offer_photos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create offer_photo" do
    assert_difference('OfferPhoto.count') do
      post :create, offer_photo: { offer_id: @offer_photo.offer_id, photo: @offer_photo.photo }
    end

    assert_redirected_to offer_photo_path(assigns(:offer_photo))
  end

  test "should show offer_photo" do
    get :show, id: @offer_photo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @offer_photo
    assert_response :success
  end

  test "should update offer_photo" do
    patch :update, id: @offer_photo, offer_photo: { offer_id: @offer_photo.offer_id, photo: @offer_photo.photo }
    assert_redirected_to offer_photo_path(assigns(:offer_photo))
  end

  test "should destroy offer_photo" do
    assert_difference('OfferPhoto.count', -1) do
      delete :destroy, id: @offer_photo
    end

    assert_redirected_to offer_photos_path
  end
end
