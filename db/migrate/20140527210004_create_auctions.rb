class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.decimal :bid
      t.references :user, index: true
      t.references :offer, index: true

      t.timestamps
    end
  end
end
