class CreateOfferPhotos < ActiveRecord::Migration
  def change
    create_table :offer_photos do |t|
      t.integer :offer_id
      t.string :photo

      t.timestamps
    end
  end
end
