class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :title
      t.string :cost
      t.string :picture
      t.date :start_date
      t.date :end_date
      t.string :area
      t.integer :rooms
      t.string :localization
      t.text :description

      t.timestamps
    end
  end
end
