class ShoutsController < ApplicationController
def new
    @shout = Shout.new
  end
  
  def create
    @shout = Shout.new(user_params)
    if @shout.save
      redirect_to :back
    else
      render :action =>'index'
    end
  end
  
  def index
    @shouts = Shout.all
  end

    private

    def user_params
        params.require(:shout).permit(:name, :message)
    end
end