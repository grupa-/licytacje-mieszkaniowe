class EventsController < ApplicationController
  before_filter :authenticate_user!
  helper_method :sort_column, :sort_direction
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    @search = Event.search do
      fulltext params[:search]
    end
  @events  = @search.results
  end

   def sort_column
    Event.column_names.include?(params[:sort]) ? params[:sort] : "Location"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @@event = Event.find params[:id]
    @comments = @event.comments([:draft, :published])
    @photos = @event.photos.all
  end

  # GET /events/new
  def new
    @event = Event.new
    @photo = @event.photos.build
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id 
    respond_to do |format|
      if @event.save
        if params[:photos]
          params[:photos]['photo'].each do |a|
            @photo = @event.photos.create!(:photo => a, :event_id => @event.id)
          end
        end
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        if params[:photos]
          params[:photos]['photo'].each do |a|
            @photo = @event.photos.create!(:photo => a, :event_id => @event.id)
          end
        end
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  def likeit
    @@event.liked_by current_user
    redirect_to @@event
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :picture, :start_date, :start_time, :end_time, :location, :user_id, :city, photos_attributes: [:id, :event_id, :photo])
    end
end