class HomeController < ApplicationController 
	before_action :set_event, only: [:show, :edit, :update, :destroy] 
	def index 
		@events = Event.all
	    @shouts = Shout.all.limit(5).order('id DESC')
	end 
	def new
		@shout = Shout.new
	end
  
  def create
	@events = Event.all
	@shouts = Shout.all.limit(10).order('id DESC')

    @shout = Shout.new(params[:shout])
    if @shout.save
      redirect_to @shout
    else
      render :index
    end
  end
end
