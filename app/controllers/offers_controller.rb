class OffersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_filter :authenticate_user!
  before_action :set_offer, only: [:show, :edit, :update, :destroy]

  # GET /offers
  # GET /offers.json
  def index
    @search = Offer.search do
      fulltext params[:search]
    end
  @offers  = @search.results
  end

  def sort_column
    Offer.column_names.include?(params[:sort]) ? params[:sort] : "Rooms"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end


  # GET /offers/1
  # GET /offers/1.json
  def show
    @photos = @offer.offer_photos.all
  end

  # GET /offers/new
  def new
    @offer = Offer.new
    @photo = @offer.offer_photos.build
  end

  # GET /offers/1/edit
  def edit
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(offer_params)
    @offer.user_id = current_user.id 

    respond_to do |format|
      if @offer.save
        if params[:offer_photos]
          params[:offer_photos]['photo'].each do |a|
            @photo = @offer.offer_photos.create!(:photo => a, :offer_id => @offer.id)
          end
        end
        format.html { redirect_to @offer, notice: 'Offer was successfully created.' }
        format.json { render :show, status: :created, location: @offer }
      else
        format.html { render :new }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offers/1
  # PATCH/PUT /offers/1.json
  def update
    respond_to do |format|
      if @offer.update(offer_params)
        if params[:offer_photos]
          params[:offer_photos]['photo'].each do |a|
            @photo = @offer.offer_photos.create!(:photo => a, :offer_id => @offer.id)
          end
        end
        format.html { redirect_to @offer, notice: 'Offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @offer }
      else
        format.html { render :edit }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer.destroy
    respond_to do |format|
      format.html { redirect_to offers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_params
      params.require(:offer).permit(:title, :cost, :picture, :start_date, :end_date, :area, :rooms, :localization, :description, offer_photos_attributes: [:id, :offer_id, :photo])
    end
end
