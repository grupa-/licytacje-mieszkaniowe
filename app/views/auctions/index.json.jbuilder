json.array!(@auctions) do |auction|
  json.extract! auction, :id, :bid, :user_id, :offer_id
  json.url auction_url(auction, format: :json)
end
