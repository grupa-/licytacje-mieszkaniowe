class Auction < ActiveRecord::Base
  belongs_to :user
  belongs_to :offer

  validate :higher?

 def higher?
   self.errors.add(:bid, "your price is too low") unless self.bid > Auction.maximum(:bid)
 end

end
