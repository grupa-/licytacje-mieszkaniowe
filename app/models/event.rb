class Event < ActiveRecord::Base
	acts_as_commontable
#	mount_uploader :picture, PictureUploader
	belongs_to :user, counter_cache: true
	acts_as_votable
	has_many :photos
	accepts_nested_attributes_for :photos

	searchable do
    	text :title, :description, :location, :city
  	end
end
