class Photo < ActiveRecord::Base
	mount_uploader :photo, PictureUploader
	belongs_to :event
end
